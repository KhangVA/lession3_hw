﻿namespace Lesson8_HW;
class Program
{
    static void Main(string[] args)
    {
        //HW1
        var hw1 = new HW1();
        hw1.MoneyConvert();


        //HW2
        var listAnimal = new List<Animals> {
        new Dog { Name ="dogName1", Color= "Yellow"},
        new Dog { Name = "dogName2", Color = "Green" },
        new Cat { Name = "catName1", Color = "Black" },
        new Cat { Name = "catName2", Color = "White" },
        new Dog { Name = "dogName3", Color = "Pink" },
        new Cat { Name = "catName3", Color = "Red" },

};
        var getDog = listAnimal.Where(animal => animal is Dog).ToList();
        foreach (var dog in getDog)
        {
            Console.WriteLine($"{dog.Name} - {dog.Color}");
        }


        //HW3
        var superMan = new SuperMan("Spider");
        var monter = new Monster("Batman");

        Console.WriteLine("Enter time Superman vs Monster: ");
        bool check = int.TryParse(Console.ReadLine(), out int number) && number > 0;

        while (!check)
        {
            Console.WriteLine("Ener number of time greater than 0, try again: ");
            check = int.TryParse(Console.ReadLine(), out number) && number > 0;
        }

        for (int i = 1; i <= number; i++)
        {
            Console.WriteLine($"-----------Attach {i} Times");
            superMan.Attack(monter);
        }

        Console.ReadKey();
    }
}
