﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson8_HW
{
    public abstract class Animals
    {
        public Animals() { }
        public string Name { get; set; }
        public string Color { get; set; }
        public abstract string Sound();
    }

    public class Cat : Animals
    {
        public Cat()
        {

        }

        public override string Sound() => "Meo Meo";

    }
    public class Dog : Animals
    {
        public override string Sound() => "Gau Gau";

    }

}


