﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson8_HW
{
    public class SuperMan
    {
        public string Name { get; set; }
        public int Hp { get; set; }

        public SuperMan(string _Name)
        {
            Name = _Name;
            Hp = 100;
        }

        public void Attack(Monster monster)
        {

            Hp -= 1;
            monster.Hp -= 10;

            if (monster.Hp == 0 || Hp == 0)
            {
                Console.WriteLine("Monster / SuperMan is die");

            }
            else
            {
                Console.WriteLine($"{Name} SupperMan - {Hp} HP");
                Console.WriteLine($"{monster.Name} Monster - {monster.Hp} HP");
            }
        }
    }
    public class Monster : SuperMan
    {
        public Monster(string _Name) : base(_Name)
        {
        }
    }
}

