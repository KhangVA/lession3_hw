﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Lesson8_HW
{
    public class HW1
    {
        public int[] denominations = { 500, 200, 100, 50, 20, 10, 5 };
        public void MoneyConvert()
        {
            Console.Write("Enter the money you want to change: ");
            int amount = int.Parse(Console.ReadLine());

            while (amount > 0)
            {
                foreach(int item in denominations)
                {
                    int count = amount / item;
                    if (count > 0)
                    {
                        Console.WriteLine($"{count} x {item}");
                        amount %= item;
                    }
                }
            }    
        } 



    }
}
