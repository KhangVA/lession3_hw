﻿/*Tạo list các số nguyên chưa có phần tử.
Add lần lượt 10 số nguyên vào list
Từ list đã cho tạo ra 1 list mới gồm các số lẻ và lớn hơn 5, 
sau đó sắp xếp theo thứ tự giảm dần*/

namespace Lesson7_HW
{
    internal class Program
    {
        static void Main(string[] args)
        
        {
            var number = new List<int>();
            var addNum = new int[10] { 11, 2, 4, 5, 7, 1, 3, 9, 12, 17 };

            number.AddRange(addNum);

            var numberList = number.Where(i => i % 2 != 0 && i > 5)
                            .OrderByDescending(i => i)
                            .ToList();
            Console.Write("Thu tu giam dan: ");

            foreach (var num in numberList)
            {
                Console.Write(num + " ");
            }

            Console.ReadLine();

        }
    }
}